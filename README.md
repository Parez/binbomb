#Setup

Run `npm install` and start Angular app as usual.

# Binbomb

Time spent:

1. UI design in Adobe XD (1h)

2. High Level Architecture thinking (2h)

3. Design/implement models and NGRX actions (2h)

3. Implement game UI structure (2h)

4. Implement reducers (3h)

5. Implement effects (2h)

6. Implement game loop (2h)

7. Debugging and testing (2h)


This is still quite buggy implementation. Also, while developing the structure evolved better and I would refactor some actions/handlers.

16 hours total spent on the course of 3 days.
Probably I should've focused more on the essential parts instead of messing with gameplay.

p.s.
You can play with game-constants.ts to change various in-game behaviours.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
