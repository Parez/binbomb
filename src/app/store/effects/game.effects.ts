import { Injectable } from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {
  distinct,
  filter, flatMap,
  map,
  mergeMap,
  switchMap,
  switchMapTo, take, takeUntil,
  tap,
} from 'rxjs/operators';
import * as BinsActions from '../actions/bins.actions';
import * as GameActions from '../actions/game.actions';
import * as BombsActions from '../actions/bombs.actions';
import {concat, of} from 'rxjs';
import * as fromRoot from '../reducers/index';
import * as fromGame from '../reducers/game.reducer';
import {select, Store} from '@ngrx/store';
import {GameService} from '../../modules/game/game.service';
import {shuffleArray} from '../../utils';
import {binColors, binsSwapTime, gameEndCondition} from '../../modules/game/game-constants';
import {Bomb} from '../../models/bomb.model';
import {fromArray} from 'rxjs/internal/observable/fromArray';

@Injectable()
export class GameEffects {
  dropBomb$ = createEffect(() => this.actions$.pipe(
    ofType(BinsActions.dropBombToBin),
    mergeMap(({bin, bomb}) => {
      if (bin.color === bomb.color) {
        return concat(
          of(BinsActions.droppedCorrectly({id: bin.id})),
          of(BombsActions.removeBomb({id: bomb.id})),
        );
      } else {
        return concat(
          of(BinsActions.droppedIncorrectly({id: bin.id})),
          of(BombsActions.removeBomb({id: bomb.id})),
        );
      }
    })
  ));
  swapTimer$ = createEffect(() => this.actions$.pipe(
    ofType(GameActions.gameLoop),
    switchMap(() => this.store.select(fromRoot.selectSwapTimer)),
    filter((timeLeft) => timeLeft === 0),
    mergeMap(() => {
      return concat(
        of(BinsActions.swapBins({colors: shuffleArray(binColors)})),
        of(GameActions.resetBinsSwapTimer({time: binsSwapTime}))
      );
    })
  ));
  explodeBomb$ = createEffect(() => this.actions$.pipe(
    ofType(GameActions.gameLoop),
    tap(console.log),
    switchMap(() => this.store.select(fromRoot.selectAllBombs)),
    map((bombs: Bomb[]) => bombs.filter(bomb => bomb.lifetime <= 0)),
    flatMap((bombs: Bomb[]) => {
      return fromArray(bombs.map((bomb) => BombsActions.explodeBomb({id: bomb.id})));
    }),
    distinct((bomb) => bomb.id) // make sure one bomb explodes only once
  ));
  createBombs$ = createEffect(() => this.actions$.pipe(
    ofType(BombsActions.createBomb),
    switchMap(() => this.gameService.createRandomBomb()),
    filter(Boolean),
    map((bomb: Bomb) => BombsActions.bombCreated({bomb})),
  ));

  // check game end condition on enterFrame after game starts
  gameEnd$ = createEffect(() => this.actions$.pipe(
    ofType(GameActions.startGame),
    switchMapTo(this.actions$.pipe(ofType(GameActions.gameLoop)).pipe(
      switchMapTo(this.store.pipe(select(fromRoot.selectGame))),
      filter(gameEndCondition),
      map((gameState: fromGame.State) => GameActions.endGame({score: gameState.score})),
      take(1)
    ))
  ));
  constructor(private actions$: Actions,
              private store: Store<fromRoot.GameState>,
              private gameService: GameService) {}
}
