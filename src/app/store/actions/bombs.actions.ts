import {createAction, props} from '@ngrx/store';
import {Bin} from '../../models/bin.model';
import {Bomb, BombPosition} from '../../models/bomb.model';

export const createBomb = createAction('[Bombs] Create');
export const bombCreated = createAction('[Bombs] Created', props<{ bomb: Bomb }>());
export const removeBomb = createAction('[Bombs] Remove', props<{ id: string }>());
export const explodeBomb = createAction('[Bombs] Explode', props<{ id: string }>());
export const dragBomb = createAction('[Bombs] Drag', props<{ id: string, pos: BombPosition }>());
export const releaseBomb = createAction('[Bombs] Release', props<{ id: string }>());
export const updateBombLifetime = createAction('[Bombs] Update Lifetime', props<{ id: string, delta: number }>());
export const revertBombXVelocity = createAction('[Bombs] Bounce of Wall', props<{ id: string }>());

