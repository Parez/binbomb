import {createAction, props} from '@ngrx/store';
import {Bomb} from '../../models/bomb.model';
import {Bin, Rectangle} from '../../models/bin.model';

export const initBins = createAction('[Bins] Init Bins', props<{ num: number, colors: string[] }>());
export const updateBinRectangle = createAction('[Bins] Update Rectangle', props<{ id: string, rect: Rectangle }>());
export const swapBins = createAction('[Bins] Swap Bins', props<{colors: string[]}>());
export const dropBombToBin = createAction('[Bins] Drop to Bin', props<{ bin: Bin, bomb: Bomb }>());
export const droppedCorrectly = createAction('[Bins] Dropped Correctly', props<{ id: string}>());
export const droppedIncorrectly = createAction('[Bins] Dropped Incorrectly', props<{ id: string}>());

