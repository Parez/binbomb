import {createAction, props} from '@ngrx/store';

export const gameLoop = createAction('[Game] Game Loop', props<{ delta: number }>());
export const startGame = createAction('[Game] Start Game');
export const endGame = createAction('[Game] End Game', props<{ score: number }>());
export const updateSpawnInterval = createAction('[Game] Update Spawn Interval', props<{ delta: number }>());
export const resetBinsSwapTimer = createAction('[Game] Reset Bins Swap Timer', props<{ time: number }>());

