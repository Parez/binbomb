import {createEntityAdapter, EntityAdapter, EntityState, Update} from '@ngrx/entity';
import {Bomb} from '../../models/bomb.model';
import {Action, createReducer, createSelector, on} from '@ngrx/store';
import * as BombsActions from '../actions/bombs.actions';
import * as GameActions from '../actions/game.actions';
import {bombGravity, bounceSlowFactor, resetVelocityOnRelease} from '../../modules/game/game-constants';
import {UpdateStr} from '@ngrx/entity/src/models';
import {addVectors, applyGravity, avgVectors, revertVectorX, scaleVector, subtractVectors} from '../../utils';

export interface State extends EntityState<Bomb> {}
const adapter: EntityAdapter<Bomb> = createEntityAdapter<Bomb>();
export const initialState: State = adapter.getInitialState();

const bombsReducer = createReducer(
  initialState,
  on(BombsActions.updateBombLifetime, (state, {id, delta}) => {
    if (id in state.entities) {
      return adapter.updateOne({id, changes: {lifetime: state.entities[id].lifetime - delta}}, state);
    }
  }),
  on(BombsActions.bombCreated, (state, {bomb}) => {
    console.log('Created');
    return adapter.addOne(bomb, state);
  }),
  on(BombsActions.dragBomb, (state, {id, pos}) => {
    return adapter.updateOne({
      id,
      changes: {
        position: pos,
        dragVector: avgVectors(subtractVectors(pos, state.entities[id].position), state.entities[id].dragVector),
        dragging: true
      }
    }, state);
  }),
  on(BombsActions.releaseBomb, (state, {id}) => {
    if (!state.entities[id].dragging) {
      return state;
    }
    return adapter.updateOne({
      id,
      changes: {dragging: false, velocityVector: state.entities[id].dragVector}
    }, state);
  }),
  on(BombsActions.removeBomb, BombsActions.explodeBomb, (state, {id}) => {
    return adapter.removeOne(id, state);
  }),
  on(BombsActions.revertBombXVelocity, (state, {id}) => {
    return adapter.updateOne({
      id,
      changes: {
        velocityVector: revertVectorX(state.entities[id].velocityVector, bounceSlowFactor)
      }
    }, state);
  }),
  on(GameActions.gameLoop, (state, {delta}) => {
    const lifetimeUpdates: UpdateStr<Bomb>[] = (state.ids as string[]).map((id) => {
      return {id, changes: {lifetime: state.entities[id].lifetime -  delta}};
    });
    const updatedLifetime: State = adapter.updateMany(lifetimeUpdates, state);
    const nonDraggedBombs: Bomb[] = Object.values(state.entities).filter(bomb => !bomb.dragging);
    const scalar: number = delta / 1000;
    const updates: UpdateStr<Bomb>[] = nonDraggedBombs.map(bomb => {
      return {
        id: bomb.id,
        changes: {
          velocityVector: applyGravity(bomb.velocityVector, bombGravity * scalar),
          position: addVectors(bomb.position, bomb.velocityVector)
        }
      };
    });
    return adapter.updateMany(updates, updatedLifetime);
  }),
  on(GameActions.endGame, (state) => {
    return initialState;
  })
);

export function reducer(state: State | undefined, action: Action) {
  return bombsReducer(state, action);
}

// get the selectors
const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal,
} = adapter.getSelectors();

// select the array of bomb ids
export const selectBombsIds = selectIds;

// select the dictionary of bomb entities
export const selectBombsEntities = selectEntities;

// select the array of bombs
export const selectAllBombs = selectAll;

// select the total bombs count
export const selectBombsTotal = selectTotal;

export const selectBombById = (id) => createSelector(
  selectBombsEntities,
  entities => entities[id]
);
