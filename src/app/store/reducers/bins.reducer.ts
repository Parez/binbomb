import {createEntityAdapter, EntityAdapter, EntityState} from '@ngrx/entity';
import {Bin} from '../../models/bin.model';
import {binColors, binsSwapTime, numBins} from '../../modules/game/game-constants';
import {generateBins, reassignBinsColors, shuffleArray} from '../../utils';
import {Action, createReducer, createSelector, on} from '@ngrx/store';
import * as BinsActions from '../actions/bins.actions';
import {selectBombsEntities} from './bombs.reducer';
import {UpdateStr} from '@ngrx/entity/src/models';
import {Bomb} from '../../models/bomb.model';
import * as GameActions from '../actions/game.actions';

export interface State extends EntityState<Bin> {}
const adapter: EntityAdapter<Bin> = createEntityAdapter<Bin>();

const bins: {[key: string]: Bin} = generateBins(numBins, binColors).reduce((acc, bin) => {
  return {...acc, [bin.id]: bin};
}, {});

export const initialState: State = {
  ids: Object.keys(bins),
  entities: bins
};

const binsReducer = createReducer(
  initialState,
  on(BinsActions.swapBins, (state) => {
    return {
      ...state,
      entities: reassignBinsColors(Object.values(state.entities), binColors)
        .reduce((acc, bin) => ({...acc, [bin.id]: bin}), {})
    };
  }),
  on(BinsActions.droppedCorrectly, (state, {id}) => {
    const bin: Bin = state.entities[id];
    return adapter.updateOne({id, changes: {numDroppedCorrect: bin.numDroppedCorrect + 1}}, state);
  }),
  on(BinsActions.droppedIncorrectly, (state, {id}) => {
    const bin: Bin = state.entities[id];
    return adapter.updateOne({
      id, changes: {
        numDroppedIncorrect: Math.min(bin.numDroppedCorrect, bin.numDroppedIncorrect + 1)}
      }, state);
  }),
  on(BinsActions.updateBinRectangle, (state, {id, rect}) => {
    return adapter.updateOne({id, changes: {rect}}, state);
  }),
  on(BinsActions.swapBins, (state, {colors}) => {
    const changes: UpdateStr<Bin>[] = (state.ids as string[])
      .map((id: string, index: number) => {
        return {id, changes: {color: colors[index]}};
      });
    return adapter.updateMany(changes, state);
  }),
  on(GameActions.endGame, (state) => {
    return initialState;
  })
);

export function reducer(state: State | undefined, action: Action) {
  return binsReducer(state, action);
}

// get the selectors
const {
  selectIds,
  selectEntities,
  selectAll,
} = adapter.getSelectors();

// select the array of bin ids
export const selectBinsIds = selectIds;

// select the dictionary of bins entities
export const selectBinsEntities = selectEntities;

// select the array of bins
export const selectAllBins = selectAll;

export const selectBinById = (id) => createSelector(
  selectBinsEntities,
  entities => entities[id]
);

export const selectBinCorrectlyDropped = (id) => createSelector(
  selectBinsEntities,
  entities => entities[id].numDroppedCorrect
);

export const selectBinIncorrectlyDropped = (id) => createSelector(
  selectBinsEntities,
  entities => entities[id].numDroppedIncorrect
);
