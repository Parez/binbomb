import {
  ActionReducerMap,
  createSelector,
  MetaReducer
} from '@ngrx/store';
import { environment } from '../../../environments/environment';
import * as bins from './bins.reducer';
import * as bombs from './bombs.reducer';
import * as game from './game.reducer';

export interface GameState {
  bins: bins.State;
  bombs: bombs.State;
  game: game.State;
}

export const reducers: ActionReducerMap<GameState> = {
  bins: bins.reducer,
  bombs: bombs.reducer,
  game: game.reducer
};

export const selectBins = (state: GameState) => state.bins;

export const selectAllBins = createSelector(
  selectBins,
  bins.selectAllBins
);

export const selectBinCorrectlyDropped = (id) => createSelector(
  selectBins,
  bins.selectBinCorrectlyDropped(id)
);

export const selectBinIncorrectlyDropped = (id) => createSelector(
  selectBins,
  bins.selectBinIncorrectlyDropped(id)
);

export const selectBinById = (id) => createSelector(
  selectBins,
  bins.selectBinById(id)
);

export const selectBombs = (state: GameState) => state.bombs;

export const selectAllBombs = createSelector(
  selectBombs,
  bombs.selectAllBombs
);

export const selectBombsDictionary = createSelector(
  selectBombs,
  bombs.selectBombsEntities
);

export const selectBombById = (id) => createSelector(
  selectBombs,
  bombs.selectBombById(id)
);

export const selectGame = (state: GameState) => state.game;
export const selectTimeElapsed = createSelector(
  selectGame,
  game.selectTimeElapsed
);
export const selectIsGameOn = createSelector(
  selectGame,
  game.selectIsGameOn
);
export const selectPlayedBefore = createSelector(
  selectGame,
  game.selectPlayedBefore
);
export const selectSpawnInterval = createSelector(
  selectGame,
  game.selectSpawnInterval
);
export const selectLifesLeft = createSelector(
  selectGame,
  game.selectLifesLeft
);
export const selectScore = createSelector(
  selectGame,
  game.selectScore
);
export const selectSwapTimer = createSelector(
  selectGame,
  game.selectSwapTimer
);

export const metaReducers: MetaReducer<GameState>[] = !environment.production ? [] : [];
