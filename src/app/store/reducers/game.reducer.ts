import {Action, createReducer, on} from '@ngrx/store';
import {
  binsSwapTime,
  bombSpawnEndInterval,
  bombSpawnIntervalDelta,
  bombSpawnStartInterval,
  initialLifes, negativeScoreAllowed,
  scoreOnCorrect,
  scoreOnExplode, scoreOnIncorrect
} from '../../modules/game/game-constants';
import * as GameActions from '../actions/game.actions';
import * as BinsActions from '../actions/bins.actions';
import * as BombsActions from '../actions/bombs.actions';
import {reassignBinsColors} from '../../utils';

export interface State {
  timeElapsed: number;
  score: number;
  totalCorrect: number;
  totalIncorrect: number;
  lifesLeft: number;
  spawnInterval: number;
  binsSwapTimer: number;
  isGameOn: boolean;
  playedBefore: boolean;
}

export const initialState: State = {
  timeElapsed: 0,
  score: 0,
  totalCorrect: 0,
  totalIncorrect: 0,
  lifesLeft: initialLifes,
  spawnInterval: bombSpawnStartInterval,
  binsSwapTimer: binsSwapTime,
  isGameOn: true,
  playedBefore: false
};

const gameReducer = createReducer(
  initialState,
  on(GameActions.startGame, (state) => {
    return {...initialState, isGameOn: true};
  }),
  on(GameActions.gameLoop, (state, {delta}) => {
    return {...state,
      timeElapsed: state.timeElapsed + delta,
      binsSwapTimer: Math.max(0, state.binsSwapTimer  - delta)
    };
  }),
  on(GameActions.resetBinsSwapTimer, (state, {time}) => {
    return {...state,
      binsSwapTimer: time
    };
  }),
  on(GameActions.updateSpawnInterval, (state, {delta}) => {
    return {
      ...state,
      spawnInterval: Math.max(state.spawnInterval + delta, bombSpawnEndInterval)
    };
  }),
  on(BinsActions.droppedCorrectly, (state) => {
    const tgScore: number = state.score + scoreOnCorrect;
    return {
      ...state,
      score: negativeScoreAllowed ? (tgScore) : Math.max(0, tgScore),
      totalCorrect: state.totalCorrect + 1
    };
  }),
  on(BinsActions.droppedIncorrectly, (state) => {
    const tgScore: number = state.score + scoreOnIncorrect;
    return {
      ...state,
      score: negativeScoreAllowed ? (tgScore) : Math.max(0, tgScore),
      totalIncorrect: state.totalIncorrect + 1
    };
  }),
  on(BombsActions.explodeBomb, (state, {id}) => {
    console.log('Explode', id);
    const tgScore: number = state.score + scoreOnExplode;
    return {...state,
      score: negativeScoreAllowed ? (tgScore) : Math.max(0, tgScore),
      lifesLeft: Math.max(0, state.lifesLeft - 1),
    };
  }),
  on(GameActions.endGame, (state) => {
    return {...state, isGameOn: false, playedBefore: true};
  })
);

export function reducer(state: State | undefined, action: Action) {
  return gameReducer(state, action);
}

export const selectSpawnInterval = (state: State) => state.spawnInterval;
export const selectTimeElapsed = (state: State) => state.timeElapsed;
export const selectScore = (state: State) => state.score;
export const selectTotalCorrect = (state: State) => state.totalCorrect;
export const selectTotalIncorrect = (state: State) => state.totalIncorrect;
export const selectLifesLeft = (state: State) => state.lifesLeft;
export const selectSwapTimer = (state: State) => state.binsSwapTimer;
export const selectIsGameOn = (state: State) => state.isGameOn;
export const selectPlayedBefore = (state: State) => state.playedBefore;
