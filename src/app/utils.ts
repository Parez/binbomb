import {Bin} from './models/bin.model';
import * as shortid from 'shortid';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Vector} from './models/bomb.model';

export interface FrameData {
  frameStartTime: number;
  deltaTime: number;
}

export const calculateStep: (prevFrame: FrameData) => Observable<FrameData> = (prevFrame: FrameData) => {
  return new Observable((observer) => {

    requestAnimationFrame((frameStartTime) => {
      // Millis to seconds
      const deltaTime = prevFrame ? (frameStartTime - prevFrame.frameStartTime) : 0;
      observer.next({
        frameStartTime,
        deltaTime
      });
    });
  });
};

export function generateBins(numBins: number, colors: string[]): Bin[] {
  const unusedColors: string[] = [...colors];
  return new Array(numBins).fill(0).map((_, index) => {
    const rndIndex: number = Math.floor(Math.random() * unusedColors.length);
    const rndColor: string = unusedColors[rndIndex];
    unusedColors.splice(rndIndex, 1);
    return {
      id: shortid.generate(),
      color: rndColor,
      numDroppedCorrect: 0,
      numDroppedIncorrect: 0
    };
  });
}

export function reassignBinsColors(bins: Bin[], colors: string[]): Bin[] {
  const unusedColors: string[] = [...colors];
  return bins.map((bin) => {
    const rndIndex: number = Math.floor(Math.random() * unusedColors.length);
    const rndColor: string = unusedColors[rndIndex];
    unusedColors.splice(rndIndex, 1);
    return {
      ...bin,
      color: rndColor
    };
  });
}

export function shuffleArray(array: any[]) {
  return [...array].sort(() => Math.random() - 0.5);
}

export function addVectors(vec1: Vector, vec2: Vector): Vector {
  return {x: vec1.x + vec2.x, y: vec1.y + vec2.y};
}

export function applyGravity(vec: Vector, gravity: number) {
  return {x: vec.x, y: vec.y + gravity};
}

export function revertVectorX(vec: Vector, slowFactor: number  = 1) {
  return {x: vec.x * slowFactor * (-1), y: vec.y};
}

export function avgVectors(vec1: Vector, vec2: Vector) {
  return {x: (vec1.x + vec2.x) / 2, y: (vec1.y + vec2.y) / 2};
}

export function scaleVector(vec: Vector, scalar: number): Vector {
  return {x: vec.x * scalar, y: vec.y * scalar};
}

export function subtractVectors(vec1: Vector, vec2: Vector): Vector {
  return {x: vec1.x - vec2.x, y: vec1.y - vec2.y};
}
