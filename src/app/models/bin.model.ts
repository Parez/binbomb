export interface Rectangle {
  left: number;
  right: number;
  top: number;
  bottom: number;
}

export interface Bin {
  id: string;
  color: string;
  numDroppedCorrect: number;
  numDroppedIncorrect: number;
  rect?: Rectangle;
}
