
export interface BombPosition {
  x: number;
  y: number;
}

export type Vector = BombPosition;

export interface Bomb {
  id: string;
  lifetime: number;
  color: string;
  position: BombPosition;
  dragging?: boolean;
  dragVector: Vector;
  velocityVector?: Vector;
  exploded?: boolean;
}
