import {ChangeDetectionStrategy, Component, HostListener, Input, OnInit} from '@angular/core';
import {Bomb, BombPosition} from '../../../models/bomb.model';
import {bombSize} from '../game-constants';
import {Store} from '@ngrx/store';
import {GameState} from '../../../store/reducers';
import {dragBomb, releaseBomb} from '../../../store/actions';
import {Bin} from '../../../models/bin.model';

@Component({
  selector: 'app-bomb',
  templateUrl: './bomb.component.html',
  styleUrls: ['./bomb.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BombComponent implements OnInit {
  @Input() bomb: Bomb;
  @Input() size: number = bombSize;

  private isDragging = false;
  private startDragMouseX: number;
  private startDragMouseY: number;
  private startDragBombX: number;
  private startDragBombY: number;
  constructor(private store: Store<GameState>) { }

  onMouseDown(event: MouseEvent): void {
    event.stopPropagation();
    this.startDragBombX = this.bomb.position.x;
    this.startDragBombY = this.bomb.position.y;
    this.startDragMouseX = event.clientX;
    this.startDragMouseY = event.clientY;
    this.isDragging = true;
    this.updatePosition(event);
  }

  @HostListener('window:mouseup', ['$event'])
  onMouseUp(event: MouseEvent): void {
    this.isDragging = false;
    this.store.dispatch(releaseBomb({id: this.bomb.id}));
  }

  @HostListener('window:mousemove', ['$event'])
  onMouseMove(event: MouseEvent): void {
    if (this.isDragging) {
      this.updatePosition(event);
    }
  }

  private updatePosition(event: MouseEvent): void {
    const newPos: BombPosition = {
      x: this.startDragBombX + (event.clientX - this.startDragMouseX),
      y: this.startDragBombY + (event.clientY - this.startDragMouseY),
    };
    this.store.dispatch(dragBomb({
      id: this.bomb.id,
      pos: newPos
    }));
  }
  ngOnInit() {
  }

}
