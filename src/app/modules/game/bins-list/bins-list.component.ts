import {Component, Input, OnInit} from '@angular/core';
import {Bin} from '../../../models/bin.model';

@Component({
  selector: 'app-bins-list',
  templateUrl: './bins-list.component.html',
  styleUrls: ['./bins-list.component.scss']
})
export class BinsListComponent implements OnInit {
  @Input() bins: Bin[] = [];
  @Input() swapTimer = 0;
  constructor() {}

  ngOnInit() {
  }

  trackById(index, item: Bin): string {
    return item.id;
  }

}
