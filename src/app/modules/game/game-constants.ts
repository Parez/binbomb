import {State} from '../../store/reducers/game.reducer';

export const binColors: string[] = ['#E05C5C', '#7EE285', '#53ACE3'];
export const numBins = 3; // should be <= binColors.length
export const bombMinLifetime = 3000;
export const bombMaxLifetime = 8000;
export const binsSwapTime = 10000;
export const scoreOnCorrect = 1;
export const scoreOnIncorrect = -1;
export const scoreOnExplode = -1;
export const initialLifes = 1;
export const bombSize = 84;
export const bombCenter = bombSize / 2;
export const bombSpawnStartInterval = 5000;
export const bombSpawnEndInterval = 500;
export const bombSpawnIntervalDelta = -60;
export const maxBombRows = 1;
export const bombGravity = 1;
export const initialBinHeight = 30;
export const binHeightGrowOnCorrect = 15;
export const binHeightGrowOnIncorrect = -15;
export const resetVelocityOnRelease = false;
export const negativeScoreAllowed = false;
export const bounceSlowFactor = 0.97;
export const statusBarHeight = 40;

export const gameEndCondition: (state: State) => boolean = (state: State) => {
  return state.timeElapsed > 120000 || (state.totalCorrect + state.totalIncorrect) >= 120 || state.lifesLeft <= 0;
};
