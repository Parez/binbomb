import { Component, OnInit } from '@angular/core';
import {Store} from '@ngrx/store';
import {GameState, selectScore, selectSwapTimer} from '../../../store/reducers';
import {Observable} from 'rxjs';
import {selectLifesLeft} from '../../../store/reducers';
import {map} from 'rxjs/operators';
import {statusBarHeight} from '../game-constants';

@Component({
  selector: 'app-status-bar',
  templateUrl: './status-bar.component.html',
  styleUrls: ['./status-bar.component.scss']
})
export class StatusBarComponent implements OnInit {
  public lifes$: Observable<number[]>;
  public score$: Observable<number>;
  public height = statusBarHeight;
  constructor(private store: Store<GameState>) {
    this.lifes$ = this.store.select(selectLifesLeft).pipe(
        map((count) => new Array(count).fill(0))
      );
    this.score$ = this.store.select(selectScore);
  }

  ngOnInit() {
  }

}
