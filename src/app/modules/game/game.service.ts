import {Injectable} from '@angular/core';
import {Bomb} from '../../models/bomb.model';
import {binColors, bombMaxLifetime, bombMinLifetime, bombSize, maxBombRows} from './game-constants';
import * as shortid from 'shortid';
import {Observable, of} from 'rxjs';

@Injectable()
export class GameService {
  private bombsPerRow: number;
  constructor() {}

  updateGrid(container: HTMLDivElement): void {
    this.bombsPerRow = Math.floor(container.clientWidth / bombSize);
  }

  createRandomBomb(): Observable<Bomb> {
    const rndIndex: number = Math.floor(Math.random() * this.bombsPerRow);
    if (!rndIndex) {
      return of(null);
    }
    return of({
      id: shortid.generate(),
      color: binColors[Math.floor(Math.random() * binColors.length)],
      position: {
        x: (rndIndex % this.bombsPerRow) * bombSize,
        y: Math.floor(rndIndex / this.bombsPerRow) * bombSize
      },
      velocityVector: {x: 0, y: 0},
      dragVector: {x: 0, y: 0},
      lifetime: Math.random() * (bombMaxLifetime - bombMinLifetime) + bombMinLifetime,
    });
  }
}
