import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GameComponent } from './game.component';
import { BinComponent } from './bin/bin.component';
import { BombComponent } from './bomb/bomb.component';
import { BombsListComponent } from './bombs-list/bombs-list.component';
import { BinsListComponent } from './bins-list/bins-list.component';
import { StatusBarComponent } from './status-bar/status-bar.component';



@NgModule({
  declarations: [GameComponent, BinComponent, BombComponent, BombsListComponent, BinsListComponent, StatusBarComponent],
  imports: [
    CommonModule
  ],
  exports: [GameComponent]
})
export class GameModule { }
