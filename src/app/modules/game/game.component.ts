import {AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {
  GameState,
  selectAllBins,
  selectAllBombs,
  selectBins, selectGame,
  selectIsGameOn,
  selectSpawnInterval,
  selectSwapTimer
} from '../../store/reducers';
import {Observable, Subject} from 'rxjs';
import {Bin} from '../../models/bin.model';
import {Bomb} from '../../models/bomb.model';
import {GameService} from './game.service';
import {createBomb, dropBombToBin, gameLoop, revertBombXVelocity, startGame, swapBins, updateSpawnInterval} from '../../store/actions';
import {binColors, binsSwapTime, bombCenter, bombSize, bombSpawnIntervalDelta, gameEndCondition, statusBarHeight} from './game-constants';
import {shuffleArray} from '../../utils';
import {filter, skip, takeUntil} from 'rxjs/operators';
import {Router} from '@angular/router';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements AfterViewInit, OnInit, OnDestroy {
  @ViewChild('container', {static: true}) container: ElementRef<HTMLDivElement>;
  public bins: Bin[];
  public bombs: Bomb[];
  public swapTimer$: Observable<number>;

  private prevTime;
  private prevSpawnTime = 0;
  private curSpawnInterval: number;

  private prevSwapTime = 0;
  private firstSpawn = true;
  private destroy$: Subject<void> = new Subject();
  private loop: number;
  constructor(private store: Store<GameState>,
              private gameService: GameService,
              private router: Router) {
    this.store.dispatch(startGame());
  }

  ngOnInit(): void {
    this.store.select(selectAllBins).pipe(takeUntil(this.destroy$))
      .subscribe(bins => {
        this.bins = bins;
      });
    this.store.select(selectAllBombs).pipe(takeUntil(this.destroy$))
      .subscribe((bombs) => {
        this.bombs = bombs;
      });
    this.store.select(selectSpawnInterval).pipe(takeUntil(this.destroy$))
      .subscribe(interval => {
        this.curSpawnInterval = interval;
      });
    this.store.select(selectIsGameOn).pipe(takeUntil(this.destroy$)).subscribe((isOn) => {
      if (!isOn) {
        this.router.navigate(['menu']);
      } else {
        this.prevTime = 0;
        this.gameLoop(0);
      }
    });
    this.swapTimer$ = this.store.select(selectSwapTimer);
  }

  ngAfterViewInit(): void {
    this.gameService.updateGrid(this.container.nativeElement);
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    window.cancelAnimationFrame(this.loop);
  }

  private gameLoop(timestamp: number): void {
    if (!this.prevTime) {
      this.prevTime = timestamp;
    }
    const deltaTime: number = timestamp - this.prevTime;
    this.store.dispatch(gameLoop({delta: deltaTime}));
    if ((this.prevSpawnTime >= this.curSpawnInterval) || this.bombs.length === 0) {
      this.store.dispatch(createBomb());
      this.store.dispatch(updateSpawnInterval({delta: bombSpawnIntervalDelta}));
      this.prevSpawnTime = 0;
      this.firstSpawn = false;
    }

    if (this.prevSwapTime >= binsSwapTime) {
      this.store.dispatch(swapBins({colors: shuffleArray(binColors)}));
      this.prevSwapTime = 0;
    }

    this.checkCollisions();
    this.prevSpawnTime += deltaTime;
    this.prevSwapTime += deltaTime;
    this.prevTime = timestamp;
    this.loop = window.requestAnimationFrame((time) => {
      console.log('request', time);
      this.gameLoop(time);
    });
  }

  private checkCollisions(): void {
    this.bombs.forEach((bomb) => {
      this.bins.filter((bin) => bin.rect).forEach((bin) => {
        if (bomb.position.y + bombCenter + statusBarHeight > bin.rect.top) {
          if (bomb.position.x + bombCenter > bin.rect.left && bomb.position.x + bombCenter < bin.rect.right) {
            this.store.dispatch(dropBombToBin({bin, bomb}));
          }
        } else {
          if (bomb.position.x < 0 || bomb.position.x + bombSize > this.container.nativeElement.clientWidth) {
            this.store.dispatch(revertBombXVelocity({id: bomb.id}));
          }
        }
      });
    });
  }
}
