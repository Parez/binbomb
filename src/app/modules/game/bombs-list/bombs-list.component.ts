import {Component, Input, OnInit} from '@angular/core';
import {Bomb} from '../../../models/bomb.model';

@Component({
  selector: 'app-bombs-list',
  templateUrl: './bombs-list.component.html',
  styleUrls: ['./bombs-list.component.scss']
})
export class BombsListComponent implements OnInit {
  @Input() bombs: Bomb[] = [];
  constructor() { }

  ngOnInit() {
  }

  trackById(index, item: Bomb): string {
    return item.id;
  }
}
