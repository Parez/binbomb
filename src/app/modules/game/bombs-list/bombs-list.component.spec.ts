import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BombsListComponent } from './bombs-list.component';

describe('BombsListComponent', () => {
  let component: BombsListComponent;
  let fixture: ComponentFixture<BombsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BombsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BombsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
