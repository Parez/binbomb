import {AfterViewInit, Component, ElementRef, HostListener, Input, OnInit, ViewChild} from '@angular/core';
import {Bin, Rectangle} from '../../../models/bin.model';
import {GameState, selectBinIncorrectlyDropped} from '../../../store/reducers';
import {Store} from '@ngrx/store';
import {updateBinRectangle} from '../../../store/actions';
import {selectBinCorrectlyDropped} from '../../../store/reducers';
import {binHeightGrowOnCorrect, binHeightGrowOnIncorrect, initialBinHeight} from '../game-constants';
import {combineLatest, concat, merge} from 'rxjs';
import {concatMap} from 'rxjs/operators';

@Component({
  selector: 'app-bin',
  templateUrl: './bin.component.html',
  styleUrls: ['./bin.component.scss']
})
export class BinComponent implements AfterViewInit, OnInit {
  @ViewChild('binElem', {static: false}) binElem: ElementRef<HTMLDivElement>;
  @Input() bin: Bin;
  public height: number;
  constructor(private store: Store<GameState>) { }

  @HostListener('window:resize')
  onResize(): void {
    this.updateDimensions();
  }

  ngOnInit(): void {
    combineLatest(
      this.store.select(selectBinCorrectlyDropped(this.bin.id)),
      this.store.select(selectBinIncorrectlyDropped(this.bin.id))
    ).subscribe(([correct, incorrect]) => {
      this.height = correct * binHeightGrowOnCorrect + incorrect * binHeightGrowOnIncorrect + initialBinHeight;
      this.updateDimensions();
    });
  }

  ngAfterViewInit(): void {
    this.updateDimensions();
  }

  private updateDimensions(): void {
    if (this.binElem) {
      const {left, right, top, bottom} = this.binElem.nativeElement.getBoundingClientRect();
      const rect: Rectangle = {left, right, top, bottom};
      this.store.dispatch(updateBinRectangle({id: this.bin.id, rect}));
    }
  }
}
