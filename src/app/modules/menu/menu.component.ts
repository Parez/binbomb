import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {Store} from '@ngrx/store';
import {GameState, selectPlayedBefore, selectScore} from '../../store/reducers';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  public score$: Observable<number>;
  public playedBefore$: Observable<boolean>;
  constructor(private router: Router, private store: Store<GameState>) {
    this.score$ = this.store.select(selectScore);
    this.playedBefore$ = this.store.select(selectPlayedBefore);
  }

  ngOnInit() {
  }

  onRestart(): void {
    console.log('Game');
    this.router.navigate(['game']);
  }

}
